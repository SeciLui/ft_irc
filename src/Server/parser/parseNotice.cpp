#include <Server.hpp>
#include <returnMessages.hpp>

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

Result Server::SearchClientsFromNickname_bis(std::vector<std::string> neededClients, std::vector<Client*>& result, Client* sender, const std::string message)
{
    for (size_t i_neededClients = 0; i_neededClients < neededClients.size(); i_neededClients++) {
        if (IsCorrectChannelName(neededClients[i_neededClients])) {
            Channel* found = SearchChannelFrom(neededClients[i_neededClients], _channels);
            if (found == NULL) {
                return (failure);
            }
            const std::vector<Client*> clientsOnChannel = found->GetClients();
            for (size_t i_clientsOnChannel = 0; i_clientsOnChannel < clientsOnChannel.size(); i_clientsOnChannel++) {
                SendMessageFrom(result, clientsOnChannel[i_clientsOnChannel], sender, neededClients[i_neededClients], message);
            }

        } else if (IsCorrectClientNickname(neededClients[i_neededClients])) {
            for (size_t i_availableClients = 0; i_availableClients < _clients.size(); i_availableClients++) {
                if (_clients[i_availableClients]->GetNickname() == neededClients[i_neededClients]) {
                    SendMessageFrom(result, _clients[i_availableClients], sender, neededClients[i_neededClients], message);
                    break;
                } else if (i_availableClients == _clients.size() - 1) {
                    return (failure);
                }
            }
        } else {
            return (failure);
        }
    }
    return (success);
}

Result Server::ParseNoticeWith(const std::vector<std::string> args, Client* client)
{
    if (client->GetIsRegistered() == false)
        return (success);
    if (args.size() < 1) {
        return (failure);
    } else if (args.size() < 2) {
        return (failure);
    }
    const std::vector<std::string> string_recipients = ExtractValuesBetweenCommas(args[0]);
    std::vector<Client*> recipients;
    if (SearchClientsFromNickname(string_recipients, recipients, client, args[1]) == failure)
        return (failure);

    return (success);
}
