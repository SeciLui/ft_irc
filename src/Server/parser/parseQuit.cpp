#include <Server.hpp>

#include <unistd.h>

Result Server::ParseQuitWith(const std::vector<std::string> args, Client* client)
{
    (void)args;
    ClientDestroy(client);

    return (success);
}
