#include <Server.hpp>
#include <returnMessages.hpp>

Result Server::ParseUserWith(const std::vector<std::string> args, Client* client)
{
    if (args.size() < 4) {
        SendMessageTo(client, ERR_NEEDMOREPARAMS("USER"));
        return (failure);
    } else if (client->GetIsRegistered()) {
        SendMessageTo(client, ERR_ALREADYREGISTRED);
        return (failure);
    }
    client->SetUser(args[0], args[1], args[2], args[3]);
    client->SetIsUserOk(true);
    if (client->CheckIfClientIsRegistered())
        send(client->GetClientSocket(), RPL_WELCOME(client->GetNickname()).c_str(), RPL_WELCOME(client->GetNickname()).size(), MSG_DONTWAIT);

    return (success);
}
