#include <Server.hpp>
#include <returnMessages.hpp>

Result Server::ParseKickWith(const std::vector<std::string> args, Client* client)
{
    if (client->GetIsRegistered() == false)
        return (success);
    else if (args.size() < 2) {
        SendMessageTo(client, ERR_NEEDMOREPARAMS("KICK"));
        return (failure);
    }
    if (IsCorrectChannelName(args[0])) {
        Channel* channel_found = SearchChannelFrom(args[0], _channels);
        if (channel_found == NULL) {
            SendMessageTo(client, ERR_NOSUCHCHANNEL(args[0]));
            return (failure);
        } else if (!channel_found->IsThisClientConnected(client)) {
            SendMessageTo(client, ERR_NOTONCHANNEL(args[0]));
            return (failure);
        } else if (!channel_found->IsThisClientOperator(client)) {
            SendMessageTo(client, ERR_CHANOPRIVSNEEDED(args[0]));
            return (failure);
        }
        if (IsCorrectClientNickname(args[1])) {
            Client* client_found = SearchClientFrom(args[1], _clients);
            if (client_found == NULL) {
                SendMessageTo(client, ERR_NOSUCHNICK(args[1]));
                return (failure);
            } else if (channel_found->IsThisClientConnected(client_found) == false) {
                SendMessageTo(client, ERR_NOTONCHANNEL(args[0]));
                return (failure);
            }
            ClientLeave(channel_found, client_found);
            if (args.size() >= 3) {
                // message des args to send
            } else {
                // message par default to send
            }
        }
    }

    return (success);
}
