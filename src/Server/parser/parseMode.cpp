#include <Server.hpp>
#include <returnMessages.hpp>

#include <iostream>

Result Server::ParsePlusO(Channel* channel, const std::string user_name, Client* client)
{
    if (IsCorrectClientNickname(user_name)) {
        Client* found = SearchClientFrom(user_name, _clients);
        if (found == NULL) {
            SendMessageTo(client, ERR_NOSUCHNICK(user_name));
            return (failure);
        } else if (channel->IsThisClientConnected(found) == false) {
            SendMessageTo(client, ERR_NOTONCHANNEL(channel->GetName()));
            return (failure);
        } else if (channel->IsThisClientOperator(found) == true)
            return (success);
        else {
            channel->AddOperator(found);
            return (success);
        }
    } else {
        SendMessageTo(client, ERR_NOSUCHNICK(user_name));
        return (failure);
    }
}

Result Server::ParseMoinsO(Channel* channel, const std::string user_name, Client* client)
{
    if (IsCorrectClientNickname(user_name)) {
        Client* found = SearchClientFrom(user_name, _clients);
        if (found == NULL) {
            SendMessageTo(client, ERR_NOSUCHNICK(user_name));
            return (failure);
        } else if (channel->IsThisClientConnected(found) == false) {
            SendMessageTo(client, ERR_NOTONCHANNEL(channel->GetName()));
            return (failure);
        } else if (channel->IsThisClientOperator(found) == true) {
            channel->DeleteOperator(found);
            return (success);
        } else
            return (success);
    } else {
        SendMessageTo(client, ERR_NOSUCHNICK(user_name));
        return (failure);
    }
}

static Result ParseMoinsI(Channel* channel)
{
    channel->SetIsInviteOnly(false);
    return (success);
}

static Result ParsePlusI(Channel* channel)
{
    channel->SetIsInviteOnly(true);
    return (success);
}

Result Server::ParseModeWith(const std::vector<std::string> args, Client* client)
{
    if (client->GetIsRegistered() == false)
        return (success);
    else if (args.size() < 2) {
        SendMessageTo(client, ERR_NEEDMOREPARAMS("MODE"));
        return (failure);
    }
    if (IsCorrectChannelName(args[0])) {
        Channel* found = SearchChannelFrom(args[0], _channels);
        if (found == NULL) {
            SendMessageTo(client, ERR_NOSUCHCHANNEL(args[0]));
            return (failure);
        } else if (!found->IsThisClientConnected(client)) {
            SendMessageTo(client, ERR_NOTONCHANNEL(args[0]));
            return (failure);
        } else if (!found->IsThisClientOperator(client)) {
            SendMessageTo(client, ERR_CHANOPRIVSNEEDED(args[0]));
            return (failure);
        } else if (!args[1].compare("-i")) {
            return (ParseMoinsI(found));
        } else if (!args[1].compare("+i")) {
            return (ParsePlusI(found));
        } else if (!args[1].compare("+o")) {
            if (args.size() < 3) {
                SendMessageTo(client, ERR_NEEDMOREPARAMS("MODE"));
                return (failure);
            }
            return (ParsePlusO(found, args[2], client));
        } else if (!args[1].compare("-o")) {
            if (args.size() < 3) {
                SendMessageTo(client, ERR_NEEDMOREPARAMS("MODE"));
                return (failure);
            }
            return (ParseMoinsO(found, args[2], client));
        } else {
            SendMessageTo(client, ERR_UNKNOWNMODE(args[1]));
            return (failure);
        }
    } else {
        SendMessageTo(client, ERR_NOSUCHCHANNEL(args[0]));
        return (failure);
    }

    return (success);
}
