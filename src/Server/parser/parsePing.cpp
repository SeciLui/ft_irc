#include <Server.hpp>
#include <returnMessages.hpp>

#include <cstring>

Result Server::ParsePingWith(const std::vector<std::string> args, Client* client)
{
    send(client->GetClientSocket(), RPL_PONG(args[0]).c_str(), RPL_PONG(args[0]).size(), MSG_DONTWAIT);

    return (success);
}
