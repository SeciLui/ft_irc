#include "Channel.hpp"

const std::string Channel::GetName(void)
{
    return (_name);
}

uint Channel::GetId(void)
{
    return (_id);
}

bool Channel::GetIsInviteOnly(void)
{
    return (_isInviteOnly);
}

std::string Channel::GetTopic()
{
    return (_topic);
}

void Channel::SetIsInviteOnly(const bool set)
{
    _isInviteOnly = set;
}

void Channel::SetTopic(const std::string topic)
{
    _topic = topic;
}
