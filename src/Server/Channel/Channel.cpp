#include <Channel.hpp>

Channel::Channel(const std::string name, const uint id)
    : _id(id)
    , _name(name)
    , _topic("")
    , _isInviteOnly(false)
{
}

Channel::~Channel(void)
{
}
