#include <Channel.hpp>

#include <algorithm>
#include <iterator>

void Channel::DeleteClient(Client* client)
{
    std::vector<Client*>::iterator it;
    if ((it = std::find(_clients.begin(), _clients.end(), client)) == _clients.end())
        return;
    _clients.erase(it);
    DeleteOperator(client);
}

void Channel::DeleteOperator(Client* client)
{
    std::vector<Client*>::iterator it;
    if ((it = std::find(_operators.begin(), _operators.end(), client)) == _operators.end())
        return;
    _operators.erase(it);
}

void Channel::AddClient(Client* client)
{
    _clients.push_back(client);
}

void Channel::AddOperator(Client* client)
{
    _operators.push_back(client);
}

const std::vector<Client*> Channel::GetClients(void) const
{
    return (_clients);
}

const std::vector<Client*> Channel::GetOperators(void) const
{
    return (_operators);
}

bool Channel::IsThisClientConnected(Client* client)
{
    if (find(_clients.begin(), _clients.end(), client) == _clients.end())
        return (false);
    return (true);
}

bool Channel::IsThisClientOperator(Client* client)
{
    if (find(_operators.begin(), _operators.end(), client) == _operators.end())
        return (false);
    return (true);
}
