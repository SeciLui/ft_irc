#include <Client.hpp>

void Client::SetClientSocket(int clientSocket)
{
    _clientSocket = clientSocket;
    return;
}

int Client::GetClientSocket(void) const
{
    return (_clientSocket);
}

struct sockaddr_in* Client::GetClientAddressAddr(void)
{
    return (&_clientAddress);
}

uint* Client::GetAddrLenAddr(void)
{
    return (&_addrLen);
}

void Client::SetIpFrom_clientAddress(void)
{
    _ip = inet_ntoa(_clientAddress.sin_addr);
    return;
}

char* Client::GetIp(void)
{
    return (_ip);
}

void Client::ConcatenateBufferWith(const std::string buffer)
{
    _buffer += buffer;
}

bool Client::DoTheBufferContainACompleteLine()
{
    if (_buffer[_buffer.size() - 1] == '\n')
        return (true);
    return (false);
}

const std::string Client::GetAndClearTheBuffer(void)
{
    std::string result = _buffer;
    _buffer = "";
    return (result);
}
