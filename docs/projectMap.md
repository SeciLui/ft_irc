# FT_IRC PROJECT MAP

## Todo

- Compare to coco's
- Readme
- Meaningful log messages
- Client to client testing
- Modes
- Command parsing/exec


## Needed Keywords

### OPER \<user>\<password>
- Grants operator privileges
- 
### QUIT [leaving message]
- Quit the server
- 
### JOIN \<chan>[,\<chan>[,\<key>[,\<key>]]
- Join one [or several] channels.

### MODE [chan or nickname]
- Administrate chans or users.

### TOPIC \<chan> [topic]
- Edit a chan's topic

### NAMES [chan[,chan...]]
- Lists every visible chans and users
- Lists every users on a chan

### LIST [chan[,chan...]]
- Lists chans and their subjects

### INVITE \<nick>\<chan>
- Invite a user to a channel (if you have permission to do so).

### KICK \<chan>\<user>
