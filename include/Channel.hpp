#pragma once

#include <Client.hpp>
#include <string>
#include <vector>

class Channel {
public:
    Channel(const std::string name, const uint id);
    ~Channel(void);

private:
    uint _id;
    std::string _name;
    std::string _topic;
    std::vector<Client*> _clients;
    std::vector<Client*> _operators;
    bool _isInviteOnly;

public:
    /**
     * @brief Client manager
     */
    void DeleteClient(Client* client);
    void AddClient(Client* client);
    void AddOperator(Client* client);
    void DeleteOperator(Client* client);
    const std::vector<Client*> GetClients(void) const;
    const std::vector<Client*> GetOperators(void) const;
    bool IsThisClientConnected(Client* client);
    bool IsThisClientOperator(Client* client);

public:
    /**
     * @brief Channel manager
     */
    const std::string GetName(void);
    uint GetId(void);
    bool GetIsInviteOnly(void);
    std::string GetTopic();

    void SetIsInviteOnly(const bool set);
    void SetTopic(const std::string topic);
};
